# Cbox Theater
Una plataforma de visibilidad: Abrimos las puertas del mundo (del arte (escénico))

_Interpretamos tu trabajo coreográfico en NYC para que lo agregues a tu semblanza artística y CV_


## Quiénes somos
[Escenaconsejo](http://escenaconsejo.org) somos una organización de artistas (?) mexicanos que actualmente reside en la ciudad de Nueva York. Legalmente estamos conformados como Compañía de Artes Escénicas y Medios Digitales Interactivos S.C. **Con CBox Theater queremos compartir los _sellos de aprobación_ que nos da esta ciudad.**

## Cómo funciona
Nosotros estamos comprometidos a:
* Recibir propuestas e instrucciones coreográficas a través de nuestro formulario (ver abajo)
* Calendarizar su presentación y dar a conocer las fechas
* Preparar, adaptar, ensayar e interpretar la coreografía de acuerdo a nuestros recursos humanos y de tiempo
* Presentar la coreografía en el lugar solicitado de NYC
* Documentar la presentación del trabajo de acuerdo a lo solicitado
* Enviarte la documentación

**Todo esto lo haremos de manera gratuita**

## Qué necesitamos de ti
Al llenar nuestro formulario, te pediremos la siguiente información:
* Datos personales básicos (Nombre, correo electrónico, dirección si deseas documentación física)
* El tipo de **documentación** que solicitas. Estas son las opciones a elegir:
  * Certificado con valor curricular firmado por los directores de Compañía de Artes Escénicas y Medios Digitales Interactivos S.C.
  * Fotografías digitales a 300 DPI
  * Programa de mano impreso
  * Video en toma fija
* **Lugar(es)** donde solicitas la presentación, con una tolerancia de +-50 metros:
  * Lincoln Center
  * Carnegie Hall
  * Brooklyn Academy of Music
  * Judson Memorial Church
  * Times Square
  * Alguna estación del metro
  * Algún lugar icónico de la ciudad
  * Otro
* Un **texto** describiendo tu trabajo
* Tu **coreografía**, comunicada a través de instrucciones, partitura, video, etc. 
* Número de **intérpretes** (privilegiaremos las propuestas de 1 o 2)
* **Vestuario** basado en las siguientes opciones:
  * Ropa de calle
  * Ropa de trabajo
  * Ropa negra
  * Ropa gris
* Otras indicaciones/peticiones
  
_Toma en cuenta que tendremos que adaptar e interpretar tu trabajo de acuerdo a nuestros recursos humanos y de tiempo_
